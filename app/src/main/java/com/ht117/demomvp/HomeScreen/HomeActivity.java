package com.ht117.demomvp.HomeScreen;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ht117.demomvp.R;

public class HomeActivity extends Activity implements HomeContract.HomeView, View.OnClickListener {

    private HomeContract.HomePresenter homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        findViewById(R.id.btnAdd).setOnClickListener(this);
        findViewById(R.id.btnUpdate).setOnClickListener(this);
        homePresenter = new HomePresenterImpl(this);
    }

    @Override
    public void onAddModel(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateModel(String message) {
        Toast.makeText(this,  message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                homePresenter.addNewModel("Add model");
                break;
            case R.id.btnUpdate:
                homePresenter.updateModel("Update model");
                break;
            default:
                break;
        }
    }
}
