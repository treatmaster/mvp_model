package com.ht117.demomvp.HomeScreen;

import android.util.Log;

/**
 * Created by steve on 3/22/17.
 */

public class HomePresenterImpl implements HomeContract.HomePresenter {

    private HomeContract.HomeView homeView;

    public HomePresenterImpl(HomeContract.HomeView view) {
        this.homeView = view;
    }

    @Override
    public void addNewModel(String message) {
        Log.d("TheShitCode", "Processing " + message);
        homeView.onAddModel(message);
    }

    @Override
    public void updateModel(String message) {
        Log.d("TheShitCode", "Processing " + message);
        homeView.onUpdateModel(message);
    }
}
