package com.ht117.demomvp.HomeScreen;

import com.ht117.demomvp.BaseComp.BasePresenter;
import com.ht117.demomvp.BaseComp.BaseView;

/**
 * Created by steve on 3/22/17.
 */

public interface HomeContract {

    interface HomeView extends BaseView<HomePresenter> {
        void onAddModel(String message);
        void onUpdateModel(String message);
    }

    interface HomePresenter extends BasePresenter {
        void addNewModel(String message);
        void updateModel(String message);
    }
}
